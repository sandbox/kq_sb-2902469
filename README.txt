IMPORTANT: Normally patching of core Drupal file is highly frowned upon.
In this case the patch is the only way to implement this additional functionality, and is PURELY COSMETIC.

The quick filter system still works without running the patch.
However, without this the displayed filter text will just say " is " and not correctly display "user is xxx".

Running the patch is at your own risk, etc etc.